package MsSpnl;
import robocode.*;
import java.awt.Color;


// API help : https://robocode.sourceforge.io/docs/robocode/robocode/Robot.html

/**
 * TurnShot - a robot by Mateus Spínola
 * @author Mateus Spínola
 */
public class TurnShot extends Robot
{
	/**
	 * run: TurnShot's default behavior
	 */
	public void run() {
						
		// Initialization of the robot should be put here
		initialize();

		GoBorder();
		
		// Robot main loop
		while(true){
			SnakeMove();
		}		
	}

	/**
	 * onScannedRobot: What to do when you see another robot
	 */
	public void onScannedRobot(ScannedRobotEvent e) {
		if (e.getDistance() < 100) {
			fire(3);
		}
		else if (e.getDistance() > 99 && e.getDistance() < 300) {
			fire(1.5);
		}
		else if (e.getDistance() > 299 && e.getDistance() < 1000) {
			fire(1);
		}
		else {
			fire (.5);
		}

		final double b = e.getBearing();
		turnGunLeft(b + 10);
	}
	
	/**
	 * onHitWall: What to do when you hit a wall
	 */
	public void onHitWall(HitWallEvent e) {
		// Replace the next line with any behavior you would like
		back(300);
		turnGunLeft(360);
		ahead(100);
		GoBorder();
	}	
	
	
	private void initialize() {
		// Body moves, gun and radar don't
		setAdjustGunForRobotTurn(true);
		
		//Set Robot Colors
		setBodyColor(new Color(0XC0,	0XC0, 0XC0)); // Silver
		setGunColor(new Color(0X99,	0XCC, 0XFF)); // Light Blue
		setRadarColor(new Color(0XCC, 0XFF, 0XFF)); // Lighter Blue + a Little of Green
		setBulletColor(new Color(0XFF, 0X00, 0XFF)); // Fuchsia
		setScanColor(new Color(0XCC, 0XFF, 0XFF)); // It's the Same of the Radar color
	}
	
	private void GoBorder() {
		// Variable
		double XPos = getX(); // Position X of the robot
		double YPos = getY(); // Position Y of the robot
		double FWidth = getBattleFieldWidth(); // Width of the Field
		double FHeight = getBattleFieldHeight(); // Height of the field
		double Head = getHeading(); // Get the direction that the robot's body is facing
			
		double BordR = FWidth - XPos; // Distance Robot to Border Right
		double BordT = FHeight - YPos; // Distance Robot to border top
		
		
		// Conditions to move
		if (BordR < FWidth/2 && BordR < BordT && BordR < YPos) {
			// When TurnShot's closer to the Right side
			if (Head > 270) {
				turnRight((360 - Head) + 90);
			}
			else if (Head <= 90) {
				turnRight(90 - Head);
			}
			else {
				turnLeft(Head - 90);
			}
			
			ahead(BordR - 50);
		}
		// When TurnShot's closer to the top
		else if (BordT < FHeight/2 && BordT < BordR && BordR < XPos) {
			if (Head > 180) {
				turnRight(360 - Head);
			}
			else {
				turnLeft(Head);
			}
			
			ahead(BordT - 50);
		}
		// When TurnShot's closer to the bottom
		else if (XPos > YPos){ 
			if (Head > 270) {
				turnLeft((360 - Head) + 90);
			}
			else if (Head <= 90) {
				turnLeft(90 - Head);
			}
			else {
				turnRight(Head - 90);
			}
			
			ahead(XPos - 50);
		}
		// When TurnShot's closer to the left side
		else {
			if (Head > 180) {
				turnLeft(360 - Head);
			}
			else {
				turnRight(Head);
			}
			
			ahead(BordT - 50);
		}
		turnGunLeft(360);
		turnLeft(90);
	}
	
	private void SnakeMove() {
		turnLeft(40);
		ahead(60);
		turnGunRight(360);		
		turnRight(80);
		ahead(60);
		turnGunRight(360);
		turnLeft(40);
		for (int i = 0; i < 4; i++) {
			turnLeft(90);
			ahead(50);
			turnGunRight(360);
		}
	}
}