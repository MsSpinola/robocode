O forte do meu robô é o desvio de balas. Sem ter um rumo certo definido ele anda imitando uma cobra, ou seja, de forma sinuosa. A sua fraqueza são ataques de curta distância. Pra compensar isso, o meu robô usa o máximo de energia possível nos ataques próximos.

Para controle de energia, o robô muda a quantidade de energia usada nos tiros de acordo com a distância.

No início da partida, o robô buscará a borda mais próxima, assim ele ganha maior espaço para se movimentar.

Sua forma de atirar é baseada na detecção do oponente pelo radar. Ao detectar o inimigo, o robô dispara.